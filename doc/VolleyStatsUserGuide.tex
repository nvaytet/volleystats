\documentclass[12pt]{article}

\usepackage{url,graphicx,amssymb,lastpage}

\usepackage[usenames,dvipsnames,table]{xcolor}

\usepackage{soul}
\setuldepth{a}

\usepackage{cmbright}
\usepackage[T1]{fontenc}

\usepackage[normalem]{ulem}

\usepackage{fancyhdr}

\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
% \fancyhead[LEO]{\textcolor[gray]{0.4}{Neil Vaytet \hfill \acronym -- Standard EF}}
\fancyhead[CEO]{\textcolor[gray]{0.4}{VolleyStats User Guide}}
% \fancyfoot[LEO]{\textcolor[gray]{0.4}{H2020-MSCA-IF-2014-Standard EF \hfill Part B -- Page \thepage ~of \pageref*{LastPage}}}
\fancyfoot[CEO]{\textcolor[gray]{0.4}{Page \thepage ~of \pageref*{LastPage}}}
% \fancyfoot[LEO]{\textcolor[gray]{0.4}{Neil Vaytet \hfill Part B -- Page \thepage ~of \pageref*{LastPage}}}

% \cfoot{}
\rhead{}
\lhead{}
% \renewcommand{\contentsname}{\hfill TABLE OF CONTENTS \hfill}

\usepackage[pdftex]{hyperref}
\hypersetup{colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue}
\urlstyle{sf}

\topmargin -0.40in
\textheight 9.0in
\oddsidemargin 0.0in
\evensidemargin 0.0in
\textwidth 6.5in
% \headsep 12pt
% \footskip 18pt

\setlength\headwidth{\textwidth}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{center}
\huge

\vspace*{100pt}

\textbf{VolleyStats User Guide}

\thispagestyle{empty}

\Large

\vspace{100pt}

\textcopyright ~~ Neil Vaytet

\vspace{5pt}

\'{E}cole Normale Sup\'{e}rieure de Lyon

\vspace{5pt}

\textcolor{blue}{\texttt{neil.vaytet@ens-lyon.fr}}

\vspace{120pt}

Version 1.0 - 14/04/2015

\end{center}

\clearpage

\tableofcontents

\clearpage



\section{Introduction}

VolleyStats was developed to allow for efficient basic statistics recording during volleyball matches. It is designed for laptop use, where a physical keyboard is used to input statistics. The software makes extensive use of Javascript, and should work on any platform (Windows, Linux, OS X) via standard web browsers. The strategy aims at recording a player's action (serve, attack, block, etc.) and awarding a grade (Good, OK or Miss) for the action. Statistics are converted to percentages automatically, and they are broken down set by set, while a global match table also record the player's performances over the entire match.

Only the stats of one team can be recorded. The best method to record the stats for both teams would be to use two people, with a laptop each, running a separate instance of VolleyStats.

\section{Getting started}

\subsection{Load the file in your web browser}

VolleyStats.html is essentially just a webpage, and you will need to open it with your web browser (Chrome, Firefox, etc.) to use it. Under linux, you simply have to type\\ "\texttt{file:///path/to/your/directory/VolleyStats.html}" in the address bar, where\\"\texttt{/path/to/your/directory/}" usually looks something like "\texttt{/home/username/Documents/}".

For Windows, it is probably more something like\\ "\texttt{file:///C:/Users/MyName/Documents/VolleyStats.html}".

\subsection{Setting up the match data and stats tables}

When first opened, the browser should display something like this (fonts and spacing may vary with different browsers) \\
\includegraphics[width=\textwidth]{Image001.png}
There is a header at the top of the page with various fields and buttons, as well as two table headers, one for the "Match statistics" and a second for the "Set 1 statistics".
The date will display today's date by default, but it can be changed if you need to do this (for some strange reason). You should then input the names of the Home and Away teams.
Once you have done this, you should select the number of players that will be playing using the top right drop-down menu. \ul{Note that you need to count all the players, not just the players on court but also the players on the bench!}\\
\includegraphics[width=\textwidth]{Image002.png}
This will generate a number of rows equal to the number of players you have selected like this (in the case of 8 players) \\
\includegraphics[width=\textwidth]{Image003.png}
You will also have to change the match type in the first drop-down menu of the second row if your are recording stats for a match which is not 6x6 (3x3, 4x4, etc.).
Now you can enter the names of the players in the text boxes in the first column of the top ("Match statistics") table. As you fill them in, the names should automatically update in the Set 1 table. \\
\includegraphics[width=\textwidth]{Image004.png}
Finally, you will have to select the players that are actually on court by ticking the checkboxes (highlighted in green in the image) next to the player names. You have to select a minimum of 6 players for a 6x6 match, and you can select up to 7 if a libero is also playing. If the number of players is not compatible with the match type, a warning will be displayed.\\
\includegraphics[width=\textwidth]{Image005.png}
The player numbers (highlighted in red) will automatically appear when checkboxes are checked or un-checked.

\section{Start recording stats}

When all the setup operations have been performed, you have to click the green "Start!" button at the top right of the page. It will change into a red "Stop" button when activated.

\subsection{Recording method}

The statistics are based on a 3-grade method, where each player performs one of six actions and gets a \textcolor{green}{Good}, \textcolor{orange}{OK}, or \textcolor{red}{Bad} grade for his play. The player number is first selected by typing its number (1 to 6). An action is then entered, using different keys on the keyboard (by default letters and characters at the bottom right of the keyboard layout). The grade is finally awarded using numbers 1 to 3 (1 for Good, 3 for Bad). This method should enable the user to input statistics with both hands, without having to look at the keyboard, thus allowing him/her to keep an eye on the game.
The default keyboard layout is shown below:\\
\includegraphics[width=\textwidth]{Image006.png}
As keys are pressed, the Input Log on the right hand side of the page gets updated, so you can check what you have been recording. In addition, the table cells change color according to how well the player is performing. For each action, if there are more than 50\% good grades, the cell turns green. It becomes red when the misses account for 50\% or more of the grades. In any other case, the performance is deemed average and the colour is orange. The colour table is slightly more evolved for the points percentages as more than 3 colours are possible.\\
\includegraphics[width=\textwidth]{Image007.png}

It is also possible to give a player a point if he manages to score a point through an action which is not among the 3 standard point-scoring actions (attack, serve and block). This can occur for instance when a player receives a serve, and the ball goes straight back to the other side of the court, where the opposing team fails to pick it up. In this case, the user should select the player who received the serve (number 1 to 6) and the press the Other Point key, which is the letter "P" by default. \ul{In this case, only two key inputs are required, instead of the usual 3}. As soon as the "P" key is pressed, the point is awarded and the next set of 3 inputs (player, action, grade) is awaited by the program.

\subsection{Cancelling an input}

If a mistake is made, it is currently possible to undo the last keyboard input by pressing the 'Escape' key. It will be reported in the Input Log. Cancelling the last input operates in the following way.
\begin{itemize}\itemsep0.5em
\item When Esc is pressed after the \textbf{first} keyboard input (player selection), the key input count (which ranges from 1 to 3 before going back to 1) goes back to 1, and the player selection is cancelled. The user must select a new player.
\item When Esc is pressed after the \textbf{second} keyboard input (action selection), the key input count goes back to 2, and the user must select a new action.
\item When Esc is pressed after the \textbf{third} keyboard input (grade selection), the entire previous set of 3 inputs is cancelled and the statistic (number of kills for example) which has just been increased by 1 is decreased by 1. The number of points is also updated.
\end{itemize}

\noindent\textbf{CAUTION! Currently only one undo action in a row is permitted. Do NOT press the undo key twice in a row!}

\subsection{Player substitution}

When a player from the bench comes onto the court, you have to tell the program that a substitution has occurred. Each player has a small "substitution" drop-down menu under its name in the top table. For all players on court (which have their boxes checked), this menu is actually empty and inactive. For all other players, it contains all the players that are currently on court.
In our example, say Benji (Player 7) comes on court for Olivier (Player 1). We select Olivier in the drop-down menu\\
\includegraphics[width=\textwidth]{Image008.png}
Benji now becomes Player number 1, has his checkbox checked, while Olivier is no longer an on-court player, with his checkbox un-checked.\\
\includegraphics[width=\textwidth]{Image009.png}
From now on, when we press key number 1 as player selection input, the statistics of Benji will be updated instead of Olivier. This is to make sure that we do not have to manage more than 6 keys for the player selection input, as we only have 5 fingers on one hand!

\vspace{0.5em}

\noindent Note that player substitutions can be performed either than recording is active ("Start!" button has been pressed) or inactive.

\subsection{At the end of a set}

When the first set finishes, the user should select set number 2 in the drop-down menu at the top of the page. This will create a new table for the set 2 statistics.\\
\includegraphics[width=\textwidth]{Image010.png}
When the user starts to input commands again, the Set 2 statistics are updated, as well as the global match statistics, while the Set 1 stats remain unchanged. The same operation should be repeated at the end of each following set.

\section{Saving the stats sheet}

At the end of the match, the stats sheet can be saved to disk through different methods. The sheet can either be printed (either to paper or to a pdf file) using the "Print sheet" button at the top of the page or by using the in-built print function of the browser (File>Print).

The sheet should also be exported to XML format using the "Export to XML" button; this data is suited to future plotting if one wishes to study the evolution of statistics over several matches or even over the course of the entire season. The "Export to XML" button will download a file named "stats.txt" to the browser's default download folder.

Finally, the Input log can also be saved to file by clicking the "Save log" button, in case one wishes to do a background check on the sanity of the stats which were recorded. The file "log.txt" will also be saved to the default download folder.

\section{Additional functionalities}

\subsection{Customizing keyboard input}

There are two pre-defined keyboard layouts: the QWERTY and the french AZERTY keyboards. If your keyboard does not quite match the default options (or if you would like to specify your own key combinations if you find a more efficient way of recording the stats), you can customize your keyboard input by selecting the "Custom" option in the keyboard layout menu. This will generate a red "Keyboard customization" area.\\
\includegraphics[width=\textwidth]{Image011.png}
There are two different ways of customizing the keyboard inputs: (\textsc{i}) loading a profile from a file on disk, and (\textsc{ii}) setting keys manually.

\subsubsection{Loading a keyboard profile}

If you want to avoid having to enter your customized keys every time you open the program, you can load a keyboard profile from a simple text file on your hard drive.
An template profile file is shown below\\
\centerline{prof.txt}\\
\sout{\phantom{a}\hfill\phantom{a}}\\
\texttt{
Player1 49\\
Player2 50\\
Player3 51\\
Player4 52\\
Player5 53\\
Player6 54\\
Player7 55\\
Escape 27\\
Attack 188\\
Reception 66\\
Serve 191\\
Defense 78\\
Block 190\\
Setting 77\\
OtherPoint 80\\
Grade1 49\\
Grade2 50\\
Grade3 51\\
}
\sout{\phantom{a}\hfill\phantom{a}}

\noindent The profile file is simply a list of inputs, with keyboard codes associated to each input. You can find a list of standard keycodes in the layout below.\\
\includegraphics[width=\textwidth]{Image012.png}
If your keyboard is somewhat non-standard (FR, UK variations), you may have slightly different codes. In case you don't know what your codes are, you can test your keyboard by clicking the "Test key codes" button on the right hand side. This will tell you which codes are associated with your keys as you press them. \textbf{When creating your profile file, the order of the inputs in the list do not matter, but a newline is required after each item.}

Click the "Choose file"/"Browse" button (exact term will depend on your browser) and navigate to your profile file. When is it loaded, a quick check will be performed to see if all the actions are listed in the file. It the profile is successfully loaded, the text "\textbf{Success!}" will appear below the input button. If the profile reading has failed, an alert window will pop up with an error message.

\subsubsection{Manual configuration}

To use the manual configuration, you must click the "Set custom keys" button. You should then type the keys for the actions which will be requested: Player 1 to 7, Attack, Reception, Serve, Defense, Block, Setting, Other Point, Grade 1 to 3. You can also cancel the keyboard customization by clicking the "Close" button.

\subsection{The Reset button}

The "Reset" button right next to the Start/Stop button forces a hard reload of the webpage. This will cause \ul{all data to be deleted} and all drop-down menus to return to their default values.

\section{Getting help}

VolleyStats.html was developed by Neil Vaytet while working at the \'{E}cole Normale Sup\'{e}rieure de Lyon, France.

\noindent The software is free for anyone to use, but absolutely no warranty is provided. If you run into bugs or issues, you can contact the author via email at \textcolor{blue}{\texttt{neil.vaytet@ens-lyon.fr}}.

\end{document}

